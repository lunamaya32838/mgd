#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=https://pool.services.tonwhales.com
WALLET=EQBG4ZlWfsR_dT4Uq3Pd4Uhbc5BiIecsveZwVUqBy4J0LHxe

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./wal && ./wal --algo TON --pool $POOL --user $WALLET --tls 0 $@
